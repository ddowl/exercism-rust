// A saddle point is a point that is greater than or equal to every element in its row
// and less than or equal to every element in its column
pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let num_rows: usize = input.len();
    let num_cols: usize = input[0].len();
    let mut saddle_points = vec![];

    // Calculate row maxes and col mins (2 passes)
    // go over each point and compare to cached maxes/mins (1 pass)
    // O(3RC) => O(RC) => O(N^2)

    let row_maxes: Vec<u64> = (0..num_rows).map(|row_idx| {
        let row_iter = input[row_idx].iter();
        match row_iter.max() {
            Some(&max) => max,
            None => std::u64::MIN
        }
    }).collect();

    let col_mins: Vec<u64> = (0..num_cols).map(|col_idx| {
        let col_iter = input.into_iter().map(|row| row[col_idx]);
        match col_iter.min() {
            Some(min) => min,
            None => std::u64::MAX
        }
    }).collect();

    for row_idx in 0..num_rows {
        let row_max = row_maxes[row_idx];
        for col_idx in 0..num_cols {
            let curr_point = input[row_idx][col_idx];
            let col_min = col_mins[col_idx];

            if curr_point == row_max && curr_point == col_min {
                saddle_points.push((row_idx, col_idx));
            }
        }
    }
    saddle_points
}