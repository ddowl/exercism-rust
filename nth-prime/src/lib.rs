pub fn nth(n: u32) -> u32 {
    let upper_bound = if n >= 6 {
        let f = n as f64;
        // https://en.wikipedia.org/wiki/Prime_number_theorem#Approximations_for_the_nth_prime_number
        (f * (f.ln() + f.ln().ln())) as usize
    } else {
        20
    };

    let mut sieve: Vec<bool> = vec![false; upper_bound];
    sieve[0] = true;
    sieve[1] = true;

    let mut nth_prime = 2;

    // Finding the ith prime
    for _ in 0..n {

        // find first open space
        nth_prime = first_open_space(&sieve, nth_prime);

        // fill in sieve
        for idx in (nth_prime..upper_bound).step_by(nth_prime) {
            sieve[idx] = true;
        }
    }

    first_open_space(&sieve, nth_prime) as u32
}

fn first_open_space(sieve: &Vec<bool>, at_least: usize) -> usize {
    let mut idx = at_least;
    while sieve[idx] { idx += 1; }
    idx
}