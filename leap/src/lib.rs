pub fn is_leap_year(year: i32) -> bool {
    divisible_by(year, 4) &&
        !divisible_by(year, 100) ||
        divisible_by(year, 400)
}

fn divisible_by(year: i32, num: i32) -> bool {
    year % num == 0
}