use std::collections::HashSet;

pub fn find(sum: u32) -> HashSet<[u32; 3]> {
    let mut triplets: HashSet<[u32; 3]> = HashSet::new();

    // The value of first element in sorted triplet will be at most sum/3.
    for i in 1..=(sum / 3) {
        // The value of second element must be at most sum/2
        for j in (i + 1)..=(sum / 2) {
            // The third element will be the remainder
            let k = sum - (i + j);

            if is_pythagorean_triplet(i, j, k) {
                triplets.insert([i, j, k]);
            }
        }
    }
    triplets
}

fn is_pythagorean_triplet(i: u32, j: u32, k: u32) -> bool {
    i * i + j * j == k * k
}
